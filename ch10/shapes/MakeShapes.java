public class MakeShapes{
    public static void main(String[] args){
        Shape[] shapes = new Shape[2];
        shapes[0] = new Square(4);
        shapes[1] = new Triangle(4,5);

        for(Shape shape : shapes){
            System.out.println(shape.getArea());
        }
        
    }
}
