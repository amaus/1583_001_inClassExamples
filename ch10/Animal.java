public class Animal{
    private String name;

    public Animal(String name){
        this.name = name;
    }

    public void speak(){
        System.out.println("...");
    }

    public void move(){
        System.out.println("~~~");
    }
}
