public class Fish extends Animal{

    public Fish(){
        super("Fish");
    }

    public void speak(){
        System.out.println("blub blub");
    }

    public void move(){
        System.out.println("The fish swims.");   
    }
}
