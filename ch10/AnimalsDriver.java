public class AnimalsDriver{
    public static void main(String[] args){
        Animal[] theAnimals = new Animal[5];
        theAnimals[0] = new Human();
        theAnimals[1] = new Animal("animal");
        theAnimals[2] = new Bird();
        theAnimals[3] = new Fish();
        theAnimals[4] = new Human();

        if(theAnimals[0] instanceof Human){
            Human theHuman = (Human)theAnimals[0];
            theHuman.jump();
        }

        for(Animal theAnimal : theAnimals){
            if(theAnimal instanceof Human){
                Human theHuman = (Human)theAnimals[0];
                theHuman.jump();
            } else{
                theAnimal.move();
            }
        }
    }
}
