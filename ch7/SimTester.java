public class SimTester{
    public static void main(String[] args){
        System.out.println("Current population: " + Sim.getPopulation());
        Sim sue = new Sim("Sue");
        System.out.println("Current population: " + Sim.getPopulation());
        Sim bob = new Sim("Bob");
        System.out.println("Current population: " + Sim.getPopulation());
        Sim joe = new Sim("Joe");
        System.out.println("Current population: " + Sim.getPopulation());

        
        System.out.println(sue.greeting());
        sue.increaseMood();
        sue.increaseMood();
        sue.increaseMood();
        sue.increaseMood();
        sue.increaseMood();
        sue.increaseMood();
        sue.increaseMood();
        sue.increaseMood();
        sue.increaseMood();
        System.out.println(sue.greeting());
        System.out.println(joe.greeting());
    }
}
