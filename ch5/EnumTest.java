public class EnumTest{
    public static enum Days {SUNDAY, MONDAY, TUESDAY, 
                        WEDNESDAY, THURSDAY, FRIDAY, SATURDAY}

    public static void main(String[] args){
        tellItLikeItIs(Days.MONDAY);
        tellItLikeItIs(Days.WEDNESDAY);
        tellItLikeItIs(Days.FRIDAY);
        tellItLikeItIs(Days.SATURDAY);
        tellItLikeItIs(Days.THURSDAY);
    }

    public static void tellItLikeItIs(Days day){
        switch(day){
            case MONDAY:
                System.out.println("Mondays suck!");
                break;
            case FRIDAY:
                System.out.println("Fridays are better");
                break;
            case WEDNESDAY:
                System.out.println("AKA humpday");
                break;
            case SATURDAY:
            case SUNDAY:
                System.out.println("Weekends are the best!");
                break;
            default:
                System.out.println("ehhh. whatever. :-|");
                break;
        }
    }
}
