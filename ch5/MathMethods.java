public class MathMethods{

    public static void main(String[] args){
        double num = max(3.14, 2.71, 4.24);
        System.out.println(num);
        int num2 = max(3, 2, 4);
        System.out.println(num2);
        
    }

    public static int max(int a, int b, int c){
        System.out.println("in method that takes ints");
        int maximum = a;
        if(b > maximum){
            maximum = b;
        }
        if(c > maximum){
            maximum = c;
        }
        return maximum;
    }

    public static double max(double a, double b, double c){
        System.out.println("in method that takes doubles");
        double maximum = a;
        if(b > maximum){
            maximum = b;
        }
        if(c > maximum){
            maximum = c;
        }
        return maximum;
    }

    public static double pow(double base, int exponent){
        double ans = 1.0;
        
        for(int i = 0; i < exponent; i++){
            ans *= base;
        }
        return ans;

    }

    public static boolean isPrime(int potentialPrime){
        boolean isPrime = true;
        for(int potentialDivisor = 2; potentialDivisor <= (Math.sqrt(potentialPrime)); potentialDivisor++){
            if(potentialPrime % potentialDivisor == 0){
                isPrime = false;
                break;
            }
        }
        return isPrime;
    }

}
