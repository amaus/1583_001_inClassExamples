import java.util.Scanner;
import java.util.Date;
public class PrimeGenerator{
    public static void main(String[] args){
        int limit;
        Scanner inputReader = new Scanner(System.in);
        int numPrimes = 0;

        System.out.print("enter limit: ");
        limit = inputReader.nextInt();
        System.out.println(limit);
        
        long start = new Date().getTime();

        for(int potentialPrime = 2; potentialPrime <= limit; potentialPrime++){
            // test if the potentialPrime is prime
            boolean isPrime = MathMethods.isPrime(potentialPrime);

            if(isPrime){
                numPrimes++;
                System.out.printf("%d\t",potentialPrime);
                if(numPrimes % 10 == 0){
                    System.out.println();
                }
            }
        }
        long end = new Date().getTime();
        System.out.println("\nTook " + (end-start) + "milliseconds to generate "+ numPrimes+" prime numbers");
    }
}
