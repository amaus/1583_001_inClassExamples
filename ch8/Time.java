/*
 * This class will act like a time stamp. once constructed, it cannot be changed.
 * setTime() will be a private method.
*/
public class Time{
    private final int HOUR ;
    private final int MINUTE;
    private final int SECOND;
    //private int secondsPastMidnight;

    public Time(int hour, int minute, int second){
        if(hour >= 0 && hour <= 23){
            this.HOUR = hour;
        } else {
            System.out.println("invalid hour, setting to 0");
            this.HOUR = 0;
        }
        if(minute >= 0 && minute <= 59){
            this.MINUTE = minute;
        } else {
            System.out.println("invalid minute, setting to 0");
            this.MINUTE = 0;
        }
        if(second >= 0 && second <= 59){
            this.SECOND = second;
        } else {
            System.out.println("invalid second, setting to 0");
            this.SECOND = 0;
        }
    }

    public Time(){
        this.HOUR = 0;
        this.MINUTE = 0;
        this.SECOND = 0;
    }

    public int getHour(){
        return this.HOUR;
    }

    public int getMinute(){
        return this.MINUTE;
    }

    public int getSecond(){
        return this.SECOND;
    }

    public String toString(){
        String state = getHour() + ":"+getMinute()+":"+getSecond();
        return state;
    }
}
