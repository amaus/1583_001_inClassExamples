public class PassByReference{
    public static void main(String[] args){
        int[] arr = {1,2,3,4,5};
        System.out.println("References");
        ArraysExample.printArray(arr);
        multiplyByTwo(arr);
        System.out.println("After multiplyByTwo");
        ArraysExample.printArray(arr);

        System.out.println("Primitives");
        int num = 2;
        System.out.println(num);
        multiplyByTwo(num);
        System.out.println("After multiplyByTwo");
        System.out.println(num);

    }

    public static void multiplyByTwo(int val){
        val *= 2;
        System.out.println(val);
    }

    public static void multiplyByTwo(int[] vals){
        for(int i= 0; i < vals.length; i++){
            vals[i] *= 2;
        }
    }
}
