import java.util.Scanner;
// to run, you must use the -ea flag to enable assertions
// java -ea Assertions
public class Assertions{
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a positive integer: ");
        int num = in.nextInt();

        assert num > 0 : "You must enter a POSITIVE integer";

        System.out.println("You entered " + num);
    }
}
