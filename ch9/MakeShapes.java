public class MakeShapes{
    public static void main(String[] args){
        Shape theShape = new Shape("Shapey", 5);
        System.out.println(theShape.toString());

        Square theSquare = new Square(4);
        System.out.println(theSquare.toString());

        Triangle theTriangle = new Triangle(4,5);
        System.out.println(theTriangle.toString());
        
    }
}
