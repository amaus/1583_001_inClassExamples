import java.util.Scanner;
public class QuadraticEquation{
    public static void main(String[] args){
        double a;
        double b;
        double c;
        double x;

        Scanner inputReader = new Scanner(System.in);
        System.out.println("This is a Quadractic Equation solver.");
        System.out.print("Enter value for a: ");
        a = inputReader.nextDouble();
        System.out.println("the value you entered it: " + a);

        System.out.print("Enter value for b: ");
        b = inputReader.nextDouble();
        System.out.println("the value you entered it: " + b);

        System.out.print("Enter value for c: ");
        c = inputReader.nextDouble();
        System.out.println("the value you entered it: " + c);

        x = (-1*b + (b*b - 4*a*c)) / (2*a);
        System.out.println("the answer is: " + x);
    }
}
